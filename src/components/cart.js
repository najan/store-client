import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Row, Col, Button } from "react-bootstrap";

//actions
import { addFlashMessage } from '../actions/flashMessages.js';

//components
import CartItems from "./cartItems";

//reucers
import "../reducers/store";

//styles
import "../styles/cart.css"

class Cart extends Component {
  constructor(props) {
    super(props);

    this.state={
    	name: '',
    	price: 0,
    	orders: JSON.parse(localStorage.getItem("Laptops")),
      isLoading: false
    }
    
    this.onSubmit = this.onSubmit.bind(this)
  }

  onSubmit(e){
  	e.preventDefault();
  	let orders = this.state.orders;
  	let {addFlashMessage} = this.props

    this.setState({
      isLoading: true
    })

  	for(var i=0; i<orders.length; i++){
  		let item = orders[i];
  			item.owner = this.props.users.id;
  			item.user = this.props.users.firstName;
  			item.email = this.props.users.emailAddress;
  			item.phone = this.props.users.phone;	  		
  			
  			var data = new FormData();
		  	data.append("name", item.name)
		  	data.append("price", item.price)
		  	data.append("owner", item.owner)
		  	data.append("user", item.user)
		  	data.append("email", item.email)
		  	data.append("phone", item.phone)

		  	let request = new XMLHttpRequest();
		  	if(i === orders.length-1){
			  	request.onreadystatechange = function() {
			        if (this.readyState === 4 && this.status === 201) {
				        addFlashMessage({
				            type: 'success',
				            text: this.responseText
				         });
                localStorage.removeItem("Laptops")
                window.location.reload();
		    		}
			    }
			}
		request.open('POST', 'https://naj-store-server.herokuapp.com/order');
		request.send(data);
	 }
  };

  render() {
    var loading = 'Checkout'
    var disabled=true;
    const order = [];
    let list = "";
    const cart = this.state.orders;
    let amount = 0;
    if (cart !== null) {
      for (const laptop in cart) {
        if (cart.hasOwnProperty(laptop)) {
         const element = cart[laptop];
          amount += element.price;
          order.push(element);
        }
      }
      list = order.map((item, index) => {
        return <CartItems item={item} key={index} />;
      });
    }

    if(this.state.orders !== null){
    	disabled = false
    }

    if(this.state.isLoading){
      loading = 'Loading...'
      disabled = true
    }

   return(
		<div className='top'>
			<h2>Shopping Cart</h2> 
			<form onSubmit = {this.onSubmit}>
			<Row>
				<Col sm={12} md={8} className='cartDetails'>
					<Row>
						<Col xs={6}>
						</Col>
						<Col xs={4}>
							<h4>Name</h4>
						</Col>
						<Col xs={2}>
							<h4>Price</h4>
						</Col>
					</Row>
					{list}				
				</Col>
				
				<Col xs={10} sm={8} md={4} className='cartTotal'>
					<h4>Cart Total</h4>
					<Row>
						<Col xs={8} className='total'>Total</Col>
						<Col xs={4}>{amount}</Col>
						<Col xs={12} className='submit'><Button type='submit' disabled={disabled} style={{backgroundColor : 'blue'}}>{loading}</Button></Col>
					</Row>
				</Col>
			</Row>
			</form>
		</div>
	)
  }
}

function mapStateToProps(state) {
  return {
    users: state.users
  };
}

export default connect(mapStateToProps,{addFlashMessage})(Cart);

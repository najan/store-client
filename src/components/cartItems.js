import React, {Component} from 'react';
import {Col, Row} from 'react-bootstrap';

//styles
import '../styles/shop.css';
import '../styles/cart.css';
import 'bootstrap3/dist/css/bootstrap.css';

class CartItems extends Component{
	render(){ 
		return(
			<Row className='items cartDetails'>
				<Col xs={6} >
					<img src={this.props.item.image} alt={this.props.name}/>
				</Col>
				<Col xs={4}>
					<h4>{this.props.item.name}</h4>
				</Col>
				<Col xs={2}>
					<h4>{this.props.item.price}</h4>
				</Col>
			</Row>	
		)
	}
}

export default CartItems;
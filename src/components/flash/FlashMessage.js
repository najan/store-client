import React from 'react';
import classnames from 'classnames';

//styles
import '../../styles/flashMessages.css'

class FlashMessage extends React.Component {
  constructor(props) {
    super(props);
    this.onClick = this.onClick.bind(this);
  }

  onClick() {
    this.props.deleteFlashMessage(this.props.message.id);
  }

  render() {
    const { id, type, text } = this.props.message;

    return (
      <div className= {classnames( 'flash', 'alert', {
        'alert-success': type === 'success',
        'alert-danger': type === 'error'
      })}>
        {text}
      </div>
    );
  }
}


export default FlashMessage;

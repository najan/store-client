import React from 'react';
import FlashMessage from './FlashMessage';
import { connect } from 'react-redux';
import { deleteFlashMessage } from '../../actions/flashMessages';

var messages;
class FlashMessagesList extends React.Component {
  render() {
    const msg = this.props.messages;
    if(msg.length !== 0 && msg !== null){
      for(var i=0; i<msg.length; i++){
        if(i===msg.length-1){
          messages = <FlashMessage key={msg[i].id} message={msg[i]} deleteFlashMessage={this.props.deleteFlashMessage} />
        }
      }
   }
    return (
      <div> {messages}</div>
    );
  }
}


function mapStateToProps(state) {
  return {
    messages: state.flashMessages
  }
}

export default connect(mapStateToProps, { deleteFlashMessage })(FlashMessagesList);

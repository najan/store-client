import React, {Component} from 'react';
import {Redirect} from 'react-router-dom';
import { connect } from 'react-redux';
import {Form, FormGroup, FormControl, InputGroup} from 'react-bootstrap'

//actions
import { login } from '../actions/authActions';
import { addFlashMessage } from '../actions/flashMessages.js';

//styles
import '../styles/login.css';
import 'bootstrap3/dist/css/bootstrap.css';

class Login extends Component{
	constructor(props){
		super(props);
		this.state = {
				emailAddress: '',
				password: '',
				isLoading : false,
				redirect : false
		}
		this.onSubmit = this.onSubmit.bind(this);
	}

	//function to change form input values based on user input
	onChange(e){
		this.setState({
			[e.target.name]  : e.target.value
		})
	}

	onSubmit(e){
		e.preventDefault();
		this.setState({
			errors: {},
			isLoading: true
		});

		//submission of user input through axios to the server using redux actions
		this.props.login(this.state).then(
			(res) => {
				this.setState({
					isLoading: false,
					redirect: true
				});
				this.props.addFlashMessage({
	            type: 'success',
	            text: res.message
	          });
			},
			(err) => {
				this.setState({
				isLoading : false
			});
			this.props.addFlashMessage({
            type: err.response.data.type,
            text: err.response.data.message
          });
			}
		)
	}

	render(){
		var loading = 'Login'
		const { emailAddress, password, isLoading } = this.state;
		if(this.state.redirect){
			return <Redirect to = '/shop'/>
		}
		if(isLoading){
			loading = 'Loading...'
		}
		return(
			<div className='top '>
				<Form action='https://naj-store-server.herokuapp.com/signin' method='POST' className='login' onSubmit={this.onSubmit}>
					<legend>Login</legend>
					<FormGroup>
						<InputGroup>
							<InputGroup.Addon>@</InputGroup.Addon>
							<FormControl type='text' name='emailAddress' value={emailAddress} onChange={(e)=>this.onChange.call(this, e)} required/>
						</InputGroup>
					</FormGroup>
					<FormGroup>
						<InputGroup>
							<InputGroup.Addon>Password</InputGroup.Addon>
							<FormControl type='password' name='password' value={password} onChange={(e)=>this.onChange.call(this, e)} required/>
						</InputGroup>
					</FormGroup>
					<FormGroup>
						<InputGroup className='submit'>
							<FormControl type='submit' value={loading} disabled={isLoading}/>
						</InputGroup>
					</FormGroup>
				</Form>
			</div>
		)
	}
}


export default connect(null, { login, addFlashMessage })(Login);

import React, {Component} from 'react';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import {Link, Redirect} from 'react-router-dom';
import {Form, FormGroup, FormControl, InputGroup, HelpBlock} from 'react-bootstrap'

//actions
import { userSignupRequest } from '../actions/signupActions';
import { addFlashMessage } from '../actions/flashMessages.js';

//styles
import '../styles/register.css';
import 'bootstrap3/dist/css/bootstrap.css';

var match;

class Register extends Component{
	constructor(props){
		super(props);
		
		this.onSubmit = this.onSubmit.bind(this);
		this.state = {
				firstName: '',
				lastName: '',
				emailAddress: '',
				address: '',
				phone: '',
				password: '',
				passwordConfirmation: '',
		        isLoading: false,
		        redirect: false
		}
	}

	onChange(e){
		this.setState({
			[e.target.name]  : e.target.value
		})
		match = null;
	}

	onSubmit(e){
		e.preventDefault();
		this.setState({ 
			isLoading: true 
		});
		//password confirmation
		if(this.state.password ===this.state.passwordConfirmation){

			//submission of user input through axios to the server using redux actions
	      	this.props.userSignupRequest(this.state).then(
	        (res) => {
	          this.props.addFlashMessage({
	            type: res.data.type,
	            text: res.data.message
	          });
	          //actions performed when user succesfully registers
	          if (res.data.type === 'success'){
		          this.setState({ 
		        	redirect: true  })
	      		}
	      		this.setState({ 
		        	isLoading: false
		        })
	        },
	        (err) => {
	        	//actions performed when an error occurs during registration
	        	this.setState({ 
		        	isLoading: false
		        })
	        	this.props.addFlashMessage({
	            type: err.response.data.type,
	            text: err.response.data.message
	          });
	    	}
	      );
      }
      else{
      	match = 'Passwords do not match';
      	this.setState({ 
	        	isLoading: false,
	        })
      }
	}

	render(){
		var loading='Register';
		const { isLoading } = this.state;
		if(this.state.redirect){
			return <Redirect to = '/login'/>
		}
		if(isLoading){
			loading = 'Loading...'
		}
		return(
			<div className='top'>
				<Form action='https://naj-store-server.herokuapp.com/signup' method='POST' className='login' onSubmit={this.onSubmit}>
					<legend>Register</legend>
					<FormGroup>
						<InputGroup>
							<InputGroup.Addon>First Name</InputGroup.Addon>
							<FormControl type='text' name='firstName' value={this.state.firstName} onChange={(e)=>this.onChange.call(this, e)} required/>
						</InputGroup>
					</FormGroup>
					<FormGroup>
						<InputGroup>
							<InputGroup.Addon>Last Name</InputGroup.Addon>
							<FormControl type='text' name='lastName' value={this.state.lastName} onChange={(e)=>this.onChange.call(this, e)} required/>
						</InputGroup>
					</FormGroup>
					<FormGroup>
						<InputGroup>
							<InputGroup.Addon>@</InputGroup.Addon>
							<FormControl type='email' name='emailAddress' value={this.state.emailAddress} onChange={(e)=>this.onChange.call(this, e)} required/>
						</InputGroup>
					</FormGroup>
					<FormGroup>
						<InputGroup>
							<InputGroup.Addon>Address</InputGroup.Addon>
							<FormControl type='text' name='address' value={this.state.address} onChange={(e)=>this.onChange.call(this, e)} required/>
						</InputGroup>
					</FormGroup>
					<FormGroup>
						<InputGroup>
							<InputGroup.Addon>+234</InputGroup.Addon>
							<FormControl type='number' name='phone' value={this.state.phone} onChange={(e)=>this.onChange.call(this, e)} required/>
						</InputGroup>
					</FormGroup>
					<FormGroup>
						<InputGroup>
							<InputGroup.Addon>Password</InputGroup.Addon>
							<FormControl type='password' name='password' value={this.state.password} onChange={(e)=>this.onChange.call(this, e)} required/>
						</InputGroup>
						<HelpBlock className='help'>Password should be atleast 6 characters long.</HelpBlock>
					</FormGroup>
					<FormGroup>
						<InputGroup>
							<InputGroup.Addon>Confirm Password</InputGroup.Addon>
							<FormControl type='password' name='passwordConfirmation' value={this.state.passwordConfirmation} onChange={(e)=>this.onChange.call(this, e)} required/>
						</InputGroup>
					</FormGroup>
					<p className='match'>{match}</p>
					<FormGroup>
						<InputGroup className='submit'>
							<FormControl type='submit' value={loading} disabled={isLoading}/>
							<Link to='/login'>Already a member?</Link>	
						</InputGroup>
					</FormGroup>
				</Form>
			</div>
		)
	}
}

function mapDispatchToProps(dispatch){
	return bindActionCreators({
		userSignupRequest: userSignupRequest, 
		addFlashMessage: addFlashMessage
	},
		dispatch)
}

export default connect(null,  mapDispatchToProps)(Register);

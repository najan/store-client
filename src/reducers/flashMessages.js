import { ADD_FLASH_MESSAGE, DELETE_FLASH_MESSAGE, DELETE_ALL_FLASH_MESSAGE } from '../actions/actionTypes';
import uuid from 'uuid/v4';
import findIndex from 'lodash/findIndex';

export default (state = [], action = {}) => {
  switch(action.type) {
    case ADD_FLASH_MESSAGE:
      return [
        ...state,
        {
          id: uuid(),
          type: action.message.type,
          text: action.message.text
        }
      ];
    case DELETE_FLASH_MESSAGE:
       return [
        ...state,
        {
          id: null,
          type: null,
          text: null
        }
      ];


    // case DELETE_ALL_FLASH_MESSAGE:
    //   state= null
    //   return state
        

    default: return state;
  }
}
